# Kubernetes Manifest 

Kubernetes manifests are YAML or JSON files that describe the desired state of the cluster.  Within that file there are a lot of parameters which can be defined. 

Normally you have a manifest for deployment which contains a number of replicas, defines names and the properties of the deployment e.g. which container to use.  

(https://kubernetes.io/docs/concepts/workloads/controllers/deployment/) 

 ```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: todo-app-deployment
  labels:
    app: todo-app
spec:
  replicas: 1
  selector:
    matchLabels:
      app: todo-app
  template:
    metadata:
      labels:
        app: todo-app
    spec:
      containers:
      - name: todo-nextjs
        image: danielradl/todo-app:latest
        ports:
        - containerPort: 3000
          name: todo
        env:
          - name: SERVER_URL
            valueFrom:
              configMapKeyRef:
                name: todo-config
                key: SERVER_URL
 ```

Some fields a mandatory and some optional, here is a list of mandatory fields: 
- apiVersion: it defines the version of the Kubernetes API 
- kind: defines what kind of resource should be created (e.g. Deployment) 
- metadata: under that field the field ‘name’ is mandatory and this here the deployment will receive their name 
- spec: this is the desired state, again with subsets. The ‘selector’ and ‘template’ field are mandatory here, selector defines how the Deployment finds the Pods it manages, and template is pointing to the template the Deployment uses to create new pods. In the template section metadata, containter name and image are defined.  

(ChatGPT) 

```yaml
apiVersion: v1
kind: Service
metadata:
  labels:
    app: todo-app
  name: todo-svc
  namespace: todo-app
spec:
  type: NodePort
  ports:
  - name: todo-svc
    port: 3000
    targetPort: 3000
    nodePort: 31000
  selector:
    app: todo-app
```

In addition, it is commonly used to have at least a second manifest file for the service. The basic structure is the same for the deployment, however you can define on which port the service is exposed. In that manifest it is defined that the service will run on port 3000, the target port, which the service request will be forwarded to and the node port where external traffic will be routed to.  


 

 